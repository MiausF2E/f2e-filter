/* eslint-disable no-template-curly-in-string */

// hack to enforce transformation to commonjs
process.env.VUE_CLI_BABEL_TRANSPILE_MODULES = true

module.exports = {
  presets: [
    '@vue/app'
  ],
  plugins: [
    /* FIXME: minify */
    /*
    ['transform-imports', {
      vuetify: {
        transform: 'vuetify/es5/components/${member}',
        preventFullImport: true
      }
    }]
    */
  ]
}
