import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import router from './router'
import store from './store'

import '@fortawesome/fontawesome-free/css/solid.css'
import '@fortawesome/fontawesome-free/css/regular.css'
import '@fortawesome/fontawesome-free/css/fontawesome.css'
import 'vuetify/src/stylus/main.styl'

Vue.config.productionTip = false

Vue.use(Vuetify, { iconfont: 'fa' })

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
