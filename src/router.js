import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/search',
      name: 'search',
      component: require('@/pages/Search').default
    },
    {
      path: '/content',
      name: 'content',
      component: require('@/pages/Content').default
    },
    {
      path: '*',
      redirect: '/search'
    }
  ]
})
