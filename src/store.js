import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function fetchData ({ query, offset, limit } = {}) {
  let queryString = '&' +
    (query ? `q=${query}&` : '') +
    (offset ? `offset=${offset}&` : '') +
    (limit ? `limit=${limit}` : '')
  queryString = queryString.replace(/&$/, '')

  return fetch(`https://data.kcg.gov.tw/api/action/datastore_search?resource_id=92290ee5-6e61-456f-80c0-249eae2fcc97${queryString}`)
    .then(Response => Response.json())
}

export default new Vuex.Store({
  state: {
    categories: [],
    classList: {
      '1': '文化類',
      '2': '生態類',
      '3': '古蹟類',
      '4': '廟宇類',
      '5': '藝街類',
      '6': '小吃/特產類',
      '7': '國家公園類',
      '8': '國家風景區類',
      '9': '休閒農業類',
      '10': '溫泉類',
      '11': '自然風景類',
      '12': '遊憩類',
      '13': '體育健身類',
      '14': '觀光工廠類',
      '15': '都會公園類',
      '16': '森林遊樂區類',
      '17': '林場類',
      '18': '其他'
    },
    currentPage: 1,
    data: [],
    pageLimit: 3,
    ready: false,
    zones: ''
  },
  getters: {
    filteredData (state) {
      return state.data
        .filter(item => {
          return (
            !state.zones ||
            item.Zone === state.zones
          ) && (
            !state.categories.length ||
            state.categories.some(key => key === item.Class1) ||
            state.categories.some(key => key === item.Class2)
          )
        })
    }
  },
  mutations: {
    AppendData (state, data) {
      state.data = state.data.concat(data)
    },
    Fetched (state) {
      state.ready = true
    },
    Fetching (state) {
      state.ready = false
    },
    SetCriteria (state, payload) {
      typeof payload === 'function'
        ? payload(state)
        : Object.assign(state, payload)
      state.currentPage = 1
    },
    Update (state, payload) {
      Object.assign(state, payload)
    }
  },
  actions: {
    Dump ({ state, commit }) {
      function recursiveDump () {
        return fetchData({ offset: state.data.length }).then(json => {
          const { result } = json
          if (!result) throw new Error('malformed api response')
          commit('AppendData', result.records)
          if (state.data.length < result.total) {
            recursiveDump()
          }
        }).catch(Reason => console.warn(Reason))
      }
      commit('Update', { data: [] })
      commit('Fetching')
      recursiveDump().then(() => commit('Fetched'))
    }
  }
})
